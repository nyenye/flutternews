import 'package:flutter/material.dart';

import 'package:flutternews/models/item/item.dart';
import 'package:flutternews/models/item/item.db.dart';

void goBack(BuildContext context) {
  if (Navigator.canPop(context)) {
    Navigator.pop(context);
  }
}

void goToItemDetails(BuildContext context, Item item) async {
  // await ItemDatabase.deleteByDisplay(ItemDisplayPage.Comment);
  // await ItemDatabase.insertBulk(item.kids, ItemDisplayPage.Comment);

  Navigator.pushNamed(context, '/item-details', arguments: {'item': item});
}
