import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';

import 'package:flutternews/providers/theme.dart';

BottomNavigationBar buildNavigationBar(
    final BuildContext context, final int selectedIndex, final onItemTapped) {
  final themeProvider = Provider.of<ThemeProvider>(context);

  return BottomNavigationBar(
    items: <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.star),
        title: Text(
          FlutterI18n.translate(context, "navigation.bottom_navbar.top"),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.save_alt),
        title: Text(
          FlutterI18n.translate(context, "navigation.bottom_navbar.newest"),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.category),
        title: Text(
          FlutterI18n.translate(context, "navigation.bottom_navbar.by_topic"),
        ),
      ),
    ],
    currentIndex: selectedIndex,
    onTap: onItemTapped,
  );
}
