import 'package:flutter/material.dart';

import 'package:flutternews/screens/index.dart' as Screens;

final String initialRoute = '';

final Map<String, WidgetBuilder> map = {
  '/': (BuildContext context) => Screens.HomeScreen(),
  '/item-details': (BuildContext context) => Screens.ItemDetailsScreen(),
};
