import 'dart:math';

// import 'package:day/day.dart';
import 'package:awesome_loader/awesome_loader.dart' show AwesomeLoader;
import 'package:flutternews/utils.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';

import 'package:flutternews/models/item/item.dart';
import 'package:flutternews/providers/theme.dart';
import 'package:flutternews/http/fetch.dart';

const double _leftBorderWidth = 6.0;
const int _commentsToLoad = 8;

final List<Color> colorLevels = [
  Colors.orangeAccent,
  Colors.redAccent,
  Colors.pinkAccent,
  Colors.purpleAccent,
  Colors.blueAccent,
  Colors.lightBlueAccent,
  Colors.greenAccent,
  Colors.lightGreenAccent,
  Colors.yellowAccent
];

class CommentThread extends StatefulWidget {
  final Item parent;

  final int level;

  final bool forceFetch;

  CommentThread(this.parent, this.level, {this.forceFetch = false});

  @override
  State<StatefulWidget> createState() {
    return _CommentThreadState();
  }
}

class _CommentThreadState extends State<CommentThread> {
  Map<int, bool> _showKidReplies = {};

  Color _colorLevel;
  Color _colorParentLevel;

  @override
  void initState() {
    super.initState();

    _colorLevel = colorLevels[widget.level % colorLevels.length];
    _colorParentLevel =
        colorLevels[max(0, widget.level - 1) % colorLevels.length];

    // _loadMoreComments();
  }

  @override
  Widget build(BuildContext context) {
    ThemeProvider theme = Provider.of<ThemeProvider>(context);
    Map<String, TextStyle> textStyles = theme.getThemedTextStyles;
    Map<String, Color> themeColors = theme.getThemedColors;

    // if (_kids.isEmpty) {
    //   return Container(
    //     alignment: Alignment.center,
    //     child: AwesomeLoader(
    //       loaderType: AwesomeLoader.AwesomeLoader4,
    //       color: _colorParentLevel,
    //     ),
    //   );
    // }

    // if (_kids == null) {
    //   return Container(
    //     alignment: Alignment.center,
    //     child: Text('Error fetching kids'),
    //   );
    // }

    final List<Widget> stack = <Widget>[];

    widget.parent.children.forEach((Item item) {
      stack.add(_buildComment(item, theme));
    });

    // if (_isLoadingComments) {
    //   stack.add(
    //     Container(
    //       alignment: Alignment.center,
    //       child: AwesomeLoader(
    //         loaderType: AwesomeLoader.AwesomeLoader4,
    //         color: _colorLevel,
    //       ),
    //     ),
    //   );
    // } else if (_kids.length < widget.parent.kids.length) {
    //   stack.add(InkWell(
    //     onTap: _loadMoreComments,
    //     child: Container(
    //       margin: EdgeInsets.only(left: _leftBorderWidth * widget.level),
    //       padding: EdgeInsets.symmetric(vertical: 12.0),
    //       color: _colorLevel,
    //       alignment: Alignment.center,
    //       child: Text("Show more", style: textStyles['buttonPrimary']),
    //     ),
    //   ));
    // }

    return Container(
      margin: EdgeInsets.only(top: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: stack,
      ),
    );
  }

  Widget _buildComment(Item item, ThemeProvider theme) {
    Map<String, TextStyle> textStyles = theme.getThemedTextStyles;

    final showReplies =
        _showKidReplies[item.id] != null && _showKidReplies[item.id];

    List<Widget> stack = <Widget>[];

    Row header = Row(
      children: <Widget>[
        Text(item.by, style: textStyles['textS']),
        Text(' - '),
        Text(
          Utils.getTimeAgo(context, item.time),
          style: textStyles['textSFaded'],
        ),
      ],
    );

    if (!showReplies && item.descendants > 0) {
      header.children.add(
        Expanded(
          child: Container(
            alignment: Alignment.centerRight,
            child: Text(
              '+${item.descendants} Replies',
              style: textStyles['textS'],
            ),
          ),
        ),
      );
    }

    List<Widget> commentStack = <Widget>[header];

    if (item.text != null) {
      commentStack.addAll(
        [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 3.0),
          ),
          Text(
            Utils.cleanHTMLInput(item.text),
            style: textStyles['comment'],
          )
        ],
      );
    }

    // if (item.kids.isNotEmpty) {
    //   commentStack.add(GestureDetector(
    //     onTap: () => _toggleShowReplies(item.id),
    //     child: Container(
    //       margin: EdgeInsets.only(top: 6.0),
    //       alignment: Alignment.centerRight,
    //       child: Text(showReplies ? "Hide replies" : "Show replies",
    //           style: textStyles['textS']),
    //     ),
    //   ));
    // }

    stack.add(
      InkWell(
        onTap: () => _toggleShowReplies(item.id),
        child: Container(
          margin:
              EdgeInsets.only(top: 3.0, left: _leftBorderWidth * widget.level),
          padding: EdgeInsets.fromLTRB(6.0, 6.0, 12.0, 6.0),
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(color: _colorLevel, width: _leftBorderWidth),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: commentStack,
          ),
        ),
      ),
    );

    if (showReplies) {
      stack.add(CommentThread(item, widget.level + 1));
    }

    return Column(
      children: stack,
    );
  }

  void _loadMoreComments() async {
    // int startIndex = _kids.length;
    // int commentsToLoad =
    //     min(widget.parent.kids.length - startIndex, _commentsToLoad);

    // setState(() {
    //   _isLoadingComments = true;
    // });

    // try {
    //   List<Item> items = await fetchItemsByIds(
    //       widget.parent.kids.sublist(startIndex, startIndex + commentsToLoad));
    //   setState(
    //     () {
    //       items.forEach((Item item) => _kids[item.id] = item);
    //       _isLoadingComments = false;
    //     },
    //   );
    // } catch (e) {
    //   setState(() {
    //     _isLoadingComments = false;
    //     _kids = null;
    //   });
    // }
  }

  void _toggleShowReplies(int itemId) {
    if (_showKidReplies[itemId] == null) _showKidReplies[itemId] = false;

    setState(() {
      _showKidReplies[itemId] = !_showKidReplies[itemId];
    });

    print(_showKidReplies[itemId]);
  }
}
