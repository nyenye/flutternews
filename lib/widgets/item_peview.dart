import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutternews/models/item/item.dart';
import 'package:flutternews/providers/theme.dart';
import 'package:flutternews/navigation/actions.dart' as NavActions;
import 'package:flutternews/utils.dart';

class ItemPreview extends StatefulWidget {
  final Item item;

  final int index;

  final bool showPosition;

  ItemPreview(this.item, this.index, {this.showPosition = false});

  @override
  State<StatefulWidget> createState() {
    return _ItemPreviewState();
  }
}

class _ItemPreviewState extends State<ItemPreview> {
  @override
  Widget build(BuildContext context) {
    Item item = widget.item;

    ThemeProvider theme = Provider.of<ThemeProvider>(context);
    Map<String, Color> themeColors = theme.getThemedColors;

    Widget positionWidget = Container();

    if (widget.showPosition) {
      positionWidget = _buildPositionWidget(item, widget.index, theme);
    }

    Widget contentWidget = _buildContentWidget(item, context, theme);

    return InkWell(
      onTap: () => NavActions.goToItemDetails(context, item),
      splashColor: themeColors['text'],
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeColors['divider']),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[positionWidget, contentWidget],
        ),
      ),
    );
  }

  Widget _buildPositionWidget(Item item, int index, ThemeProvider theme) {
    Map<String, Color> themeColors = theme.getThemedColors;

    return Container(
      width: 65.0,
      padding: EdgeInsets.all(12.0),
      alignment: Alignment.center,
      child: DefaultTextStyle(
        style: TextStyle(
            color: themeColors['primaryAccent'], fontFamily: 'Roboto'),
        child: Column(
          children: <Widget>[
            Text(
              // Ranking
              (index + 1).toString(),
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
            ),
            Divider(indent: 15.0, endIndent: 15.0),
            Text(
              // Score
              '${item.score}p',
              style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  _buildContentWidget(Item item, BuildContext context, ThemeProvider theme) {
    ThemeProvider theme = Provider.of<ThemeProvider>(context);
    Map<String, TextStyle> textStyles = theme.getThemedTextStyles;
    Map<String, Color> themeColors = theme.getThemedColors;

    String timeAgo = Utils.getTimeAgo(context, item.time);

    Uri website = item.url != null ? Uri.parse(item.url) : null;

    return Expanded(
      child: Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.fromLTRB(0, 12.0, 12.0, 12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              item.title,
              style: textStyles['textL'],
            ),
            Container(
              margin: EdgeInsets.only(top: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          website != null ? website.host : "-",
                          style: textStyles['textSFaded'],
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 1.5),
                        ),
                        Text(
                          '$timeAgo - ${item.by}',
                          style: textStyles['textSFaded'],
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.comment,
                        color: themeColors['primaryAccent'],
                        size: 14.0,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 1.5),
                      ),
                      Text(
                        item.descendants.toString(),
                        style: textStyles['textS'],
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
