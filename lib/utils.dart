import 'dart:math' show max;

import 'package:day/day.dart' show Day;
import 'package:flutter/material.dart' show BuildContext;
import 'package:flutter_i18n/flutter_i18n.dart' show FlutterI18n;
import 'package:html_unescape/html_unescape.dart';

class Utils {
  static final HtmlUnescape _unescape = HtmlUnescape();

  static String removeAllHtmlTags(String htmlInput) {
    RegExp exp1 = RegExp(r"<p/>", multiLine: true, caseSensitive: true);

    String paragraphs = htmlInput.replaceAll(exp1, '\n');

    RegExp exp2 = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return paragraphs.replaceAll(exp2, '');
  }

  static String cleanHTMLInput(String htmlInput) {
    return _unescape.convert(removeAllHtmlTags(htmlInput));
  }

  static String getTimeAgo(BuildContext context, int time) {
    // item.time is seconds, Day.fromUnix expects milliseconds (so we * 1000)
    final Day publishDate = Day.fromUnix(time * 1000);

    if (publishDate
        .isAfter(Day.fromDateTime(DateTime.now()).subtract(1, 'h'))) {
      int difference =
          max(publishDate.diff(Day.fromDateTime(DateTime.now()), 'm'), 1);
      return '$difference${FlutterI18n.translate(context, "date_time.minute_shorthand")}';
    }
    // If the publishing date has less than 24 hours...
    else if (publishDate
        .isAfter(Day.fromDateTime(DateTime.now()).subtract(1, 'd'))) {
      int difference = publishDate.diff(Day.fromDateTime(DateTime.now()), 'h');
      return '$difference${FlutterI18n.translate(context, "date_time.hour_shorthand")}';
    } else if (publishDate
        .isAfter(Day.fromDateTime(DateTime.now()).subtract(7, 'd'))) {
      int difference = publishDate.diff(Day.fromDateTime(DateTime.now()), 'd');
      return '$difference${FlutterI18n.translate(context, "date_time.day_shorthand")}';
    }

    return publishDate
        .format(FlutterI18n.translate(context, "date_time.format"));
  }
}
