import 'package:sqflite/sqflite.dart';

Database database;

const String _dbName = 'flutternews.db';

Future<void> initSQLite() async {
  try {
    String dbPath = await getDatabasesPath();

    database =
        await openDatabase('$dbPath/$_dbName', version: 1, onCreate: _onCreate);
  } catch (e) {
    throw e;
  }
}

// Database database;
_onCreate(Database db, int version) async {
  await db.execute('''
    DROP TABLE IF EXISTS Item;
  ''');

  await db.execute('''
    DROP TABLE IF EXISTS ItemLastUpdateTime;
  ''');

  // Database is created, create the table
  await db.execute('''
    CREATE TABLE IF NOT EXISTS Item (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      item_id INTEGER,
      display_page INTEGER,
      ordering INTEGER,
      json TEXT
    );
  ''');

  await db.execute('''
    CREATE UNIQUE INDEX ITEM_item_id_display_page_unique_index ON Item (item_id, display_page);
  ''');

  await db.execute('''
    CREATE TABLE IF NOT EXISTS CacheRefreshTimes (
      id INTEGER PRIMARY KEY,
      general INTEGER,
      top INTEGER,
      newest INTEGER,
      by_topic INTEGER
    );
  ''');

  await db.execute('''
    INSERT INTO CacheRefreshTimes VALUES (1, null, null, null, null);
  ''');

  // await db.execute('''
  //   CREATE TABLE IF NOT EXISTS Item (
  //     id INTEGER PRIMARY KEY,
  //     type TEXT,
  //     title TEXT,
  //     time INTEGER,
  //     deleted INTEGER,
  //     dead INTEGER,
  //     by TEXT,
  //     text TEXT,
  //     parent INTEGER,
  //     poll INTEGER,
  //     url TEXT,
  //     score INTEGER,
  //     descendants INTEGER
  //   );
  // ''');

  // await db.execute('''
  //   CREATE TABLE IF NOT EXISTS ItemKids (
  //     item_id INTEGER,
  //     kid_id INTEGER
  //   );
  // ''');

  // await db.execute('''
  //   ALTER TABLE ItemKids ADD FOREIGN KEY (item_id) REFERENCES Item;
  // ''');

  // await db.execute('''
  //   CREATE UNIQUE INDEX itemkids_item_id_kid_id_unique_index ON ItemKids (item_id, kid_id);
  // ''');

  // await db.execute('''
  //   CREATE TABLE IF NOT EXISTS PollItemOptions (
  //     item_id INTEGER,
  //     poll_option_id INTEGER,
  //     order INTEGER
  //   );
  // ''');

  // await db.execute('''
  //   ALTER TABLE PollItemOptions ADD FOREIGN KEY (item_id) REFERENCES Item;
  // ''');

  // await db.execute('''
  //   CREATE UNIQUE INDEX pollitemoptions_item_id_poll_option_id_unique_index ON PollItemOptions (item_id, poll_option_id);
  // ''');
}
