import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeProvider with ChangeNotifier {
  bool _isLightTheme = false;

  ThemeProvider();

  ThemeData get getThemeData => _isLightTheme ? lightTheme : darkTheme;

  Map<String, Color> get getThemedColors =>
      _isLightTheme ? lightColors : darkColors;

  Map<String, TextStyle> get getThemedTextStyles =>
      _isLightTheme ? lightTextStyles : darkTextStyles;

  get isLightTheme {
    return _isLightTheme;
  }

  set isLightTheme(bool value) {
    _isLightTheme = value;
    notifyListeners();
  }
}

final Map<String, Color> darkColors = {
  'primaryColor': Colors.orange,
  'primaryAccent': Colors.orangeAccent,
  'backgroundColor': Colors.grey[900],
  'canvasColor': Colors.grey[800],
  'white': Color(0xFFF2F2F2),
  'whiteFaded': Colors.white54,
  'black': Color(0xFF0D0D0D),
  'blackFaded': Colors.black54,
  'textColor': Color(0xFFF2F2F2),
  'textColorFaded': Colors.white54,
  'divider': Colors.white54
};

final darkTheme = ThemeData(
  primarySwatch: darkColors['primaryColor'],
  primaryColor: darkColors['primaryColor'],
  brightness: Brightness.dark,
  backgroundColor: darkColors['backgroundColor'],
  scaffoldBackgroundColor: darkColors['backgroundColor'],
  canvasColor: darkColors['canvasColor'],
  accentColor: darkColors['primaryAccent'],
  accentIconTheme: IconThemeData(color: darkColors['primaryAccent']),
  dividerColor: darkColors['divider'],
  cupertinoOverrideTheme: CupertinoThemeData(
    primaryColor: darkColors['primaryAccent'],
  ),
  textTheme: TextTheme(
    caption: darkTextStyles['caption'],
  ),
);

final Map<String, TextStyle> darkTextStyles = {
  // Used by the TextTheme
  'caption': TextStyle(color: darkColors['whiteFaded']),
  // Used by me
  'appBarTitle': TextStyle(color: darkColors['white']),
  'h1': TextStyle(
    color: darkColors['white'],
    fontFamily: "InriaSerif",
    fontSize: 24.0,
    fontWeight: FontWeight.bold,
  ),
  'h1p': TextStyle(
    color: darkColors['primaryColor'],
    fontFamily: "InriaSerif",
    fontSize: 24.0,
    fontWeight: FontWeight.bold,
  ),
  'h1pa': TextStyle(
    color: darkColors['primaryAccent'],
    fontFamily: "InriaSerif",
    fontSize: 24.0,
    fontWeight: FontWeight.bold,
  ),
  'h2': TextStyle(
    color: darkColors['whiteFaded'],
    fontFamily: "InriaSerif",
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
  ),
  'textS': TextStyle(
    color: darkColors['textColor'],
    fontFamily: "Roboto",
    fontSize: 12.0,
  ),
  'text': TextStyle(
    color: darkColors['textColor'],
    fontFamily: "InriaSerif",
    fontSize: 14.0,
  ),
  'textL': TextStyle(
    color: darkColors['textColor'],
    fontFamily: "InriaSerif",
    fontSize: 18.0,
  ),
  'textSFaded': TextStyle(
    color: darkColors['textColorFaded'],
    fontFamily: "Roboto",
    fontSize: 12.0,
  ),
  'textFaded': TextStyle(
    color: darkColors['textColorFaded'],
    fontFamily: "InriaSerif",
    fontSize: 14.0,
  ),
  'textLFaded': TextStyle(
    color: darkColors['textColorFaded'],
    fontFamily: "InriaSerif",
    fontSize: 18.0,
  ),
  'comment': TextStyle(
    color: darkColors['textColor'],
    fontFamily: "Roboto",
    fontWeight: FontWeight.w300,
    fontSize: 14,
  ),
  'button': TextStyle(
    color: darkColors['primaryColor'],
    fontFamily: "Roboto",
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
  ),
  'buttonPrimary': TextStyle(
    color: darkColors['white'],
    fontFamily: "Roboto",
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
  )
};

final Map<String, Color> lightColors = {
  'primaryColor': Colors.orange,
  'primaryAccent': Colors.orangeAccent,
  'backgroundColor': Colors.grey[100],
  'canvasColor': Colors.grey[300],
  'white': Color(0xFFF2F2F2),
  'whiteFaded': Colors.white54,
  'black': Color(0xFF0D0D0D),
  'blackFaded': Colors.black54,
  'textColor': Color(0xFF0D0D0D),
  'textColorFaded': Colors.black54,
  'divider': Colors.black54
};

final lightTheme = ThemeData(
  primarySwatch: lightColors['primaryColor'],
  primaryColor: lightColors['primaryColor'],
  brightness: Brightness.dark,
  backgroundColor: lightColors['backgroundColor'],
  scaffoldBackgroundColor: lightColors['backgroundColor'],
  canvasColor: lightColors['canvasColor'],
  accentColor: lightColors['primaryAccent'],
  accentIconTheme: IconThemeData(color: lightColors['primaryAccent']),
  dividerColor: lightColors['divider'],
  cupertinoOverrideTheme: CupertinoThemeData(
    primaryColor: lightColors['primaryAccent'],
  ),
  textTheme: TextTheme(
    caption: lightTextStyles['caption'],
  ),
);

final Map<String, TextStyle> lightTextStyles = {
  // Used by the TextTheme
  'caption': TextStyle(color: lightColors['blackFaded']),
  // Used by me
  'appBarTitle': TextStyle(color: lightColors['white']),
  'h1': TextStyle(
    color: lightColors['black'],
    fontFamily: "InriaSerif",
    fontSize: 22.0,
    fontWeight: FontWeight.bold,
  ),
  'h1p': TextStyle(
    color: lightColors['primaryColor'],
    fontFamily: "InriaSerif",
    fontSize: 22.0,
    fontWeight: FontWeight.bold,
  ),
  'h1pa': TextStyle(
    color: lightColors['primaryAccent'],
    fontFamily: "InriaSerif",
    fontSize: 22.0,
    fontWeight: FontWeight.bold,
  ),
  'h2': TextStyle(
    color: lightColors['blackFaded'],
    fontFamily: "InriaSerif",
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
  ),
  'textS': TextStyle(
    color: lightColors['textColor'],
    fontFamily: "Roboto",
    fontSize: 12.0,
  ),
  'text': TextStyle(
    color: lightColors['textColor'],
    fontFamily: "InriaSerif",
    fontSize: 14.0,
  ),
  'textL': TextStyle(
    color: lightColors['textColor'],
    fontFamily: "InriaSerif",
    fontSize: 18.0,
  ),
  'textSFaded': TextStyle(
    color: lightColors['textColorFaded'],
    fontFamily: "Roboto",
    fontSize: 12.0,
  ),
  'textFaded': TextStyle(
    color: lightColors['textColorFaded'],
    fontFamily: "InriaSerif",
    fontSize: 14.0,
  ),
  'textLFaded': TextStyle(
    color: lightColors['textColorFaded'],
    fontFamily: "InriaSerif",
    fontSize: 18.0,
  ),
  'comment': TextStyle(
    color: lightColors['textColor'],
    fontFamily: "Roboto",
    fontWeight: FontWeight.w300,
    fontSize: 14,
  ),
  'button': TextStyle(
    color: darkColors['primaryColor'],
    fontFamily: "Roboto",
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
  ),
  'buttonPrimary': TextStyle(
    color: darkColors['black'],
    fontFamily: "Roboto",
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
  )
};
