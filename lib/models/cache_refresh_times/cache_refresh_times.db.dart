import 'package:flutternews/models/item/item.db.dart';
import 'package:flutternews/sqlite/index.dart';

const String TABLE_CACHE = 'CacheRefreshTimes';
const String CACHE_COLUMN_GENERAL = 'general';
const String CACHE_COLUMN_TOP = 'top';
const String CACHE_COLUMN_NEWEST = 'newest';
const String CACHE_COLUMN_BY_TOPIC = 'by_topic';

class CacheRefreshTimesDatabase {
  static String getColumnByDisplay(int display) {
    switch (display) {
      case ItemDisplayPage.Top:
        return CACHE_COLUMN_TOP;
      case ItemDisplayPage.Newest:
        return CACHE_COLUMN_NEWEST;
      case ItemDisplayPage.ByTopic:
        return CACHE_COLUMN_BY_TOPIC;
      default:
        return "";
    }
  }

  static Future<int> queryColumn(String column) async {
    try {
      List<Map<String, dynamic>> maps = await database.query(
        TABLE_CACHE,
        columns: [column],
      );

      if (maps.isEmpty || maps.first[column] == null) return null;

      return maps.first[column] as int;
    } catch (e) {
      print(e);
    }

    return null;
  }

  static Future<bool> updateColumn(String column, {int value}) async {
    try {
      await database.update(
        TABLE_CACHE,
        {
          column: value ?? DateTime.now().toUtc().millisecondsSinceEpoch,
          CACHE_COLUMN_GENERAL: DateTime.now().toUtc().millisecondsSinceEpoch
        },
      );

      return true;
    } catch (e) {
      print(e);
    }

    return false;
  }
}
