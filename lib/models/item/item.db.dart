import 'dart:convert';

import 'package:flutternews/models/item/item.dart';
import 'package:flutternews/sqlite/index.dart';

const String TABLE_ITEM = 'Item';
const String ITEM_COLUMN_ID = 'id';
const String ITEM_COLUMN_ITEM_ID = 'item_id';
const String ITEM_COLUMN_DISPLAY_PAGE = 'display_page';
const String ITEM_COLUMN_ORDERING = 'ordering';
const String ITEM_COLUMN_JSON = 'json';

class ItemDisplayPage {
  static const int Top = 1;
  static const int Newest = 2;
  static const int ByTopic = 3;
  static const int Comment = 4;
}

class ItemDatabase {
  static Future<void> insert(int itemId, int display, int order) async {
    await database.insert(
      TABLE_ITEM,
      {
        ITEM_COLUMN_ITEM_ID: itemId,
        ITEM_COLUMN_DISPLAY_PAGE: display,
        ITEM_COLUMN_ORDERING: order,
        ITEM_COLUMN_JSON: null
      },
    );
  }

  static Future<bool> insertBulk(List<int> ids, int display) async {
    try {
      int order = 0;
      for (int id in ids) await insert(id, display, order++);

      return true;
    } catch (e) {
      print(e);
    }

    return false;
  }

  static Future<Item> queryById(int id) async {
    try {
      List<Map> maps = await database.query(
        TABLE_ITEM,
        columns: [ITEM_COLUMN_JSON],
        where: '$ITEM_COLUMN_ITEM_ID == ? AND $ITEM_COLUMN_JSON NOT NULL',
        whereArgs: [id],
      );

      if (maps.length <= 0) return null;

      return Item.fromJson(jsonDecode(maps[0][ITEM_COLUMN_JSON]));
    } catch (e) {
      print(e);
    }

    return null;
  }

  static Future<List<Item>> queryByIds(List<int> ids) async {
    try {
      List<Map> maps = await database.query(
        TABLE_ITEM,
        columns: [ITEM_COLUMN_ITEM_ID, ITEM_COLUMN_JSON],
        where:
            '$ITEM_COLUMN_ITEM_ID IN (${ids.toString().replaceFirst("[", "").replaceAll("]", "")}) AND $ITEM_COLUMN_JSON NOT NULL',
      );

      if (maps.length <= 0) return null;

      return maps
          .map((Map e) => Item.fromJson(jsonDecode(e[ITEM_COLUMN_JSON])))
          .toList();
    } catch (e) {
      print(e);
    }

    return null;
  }

  static Future<List<int>> queryIdsByDisplay(int display) async {
    try {
      List<Map<String, dynamic>> maps = await database.query(
        TABLE_ITEM,
        columns: [ITEM_COLUMN_ITEM_ID],
        where: '$ITEM_COLUMN_DISPLAY_PAGE == ?',
        whereArgs: [display],
        orderBy: ITEM_COLUMN_ORDERING,
      );

      if (maps.length <= 0) return null;

      return maps
          .map((Map<String, dynamic> map) => map[ITEM_COLUMN_ITEM_ID] as int)
          .toList();
    } catch (e) {
      print(e);
    }

    return null;
  }

  static Future<List<Item>> queryByDisplay(int display) async {
    try {
      List<Map<String, dynamic>> maps = await database.query(
        TABLE_ITEM,
        columns: [ITEM_COLUMN_JSON],
        where: '$ITEM_COLUMN_DISPLAY_PAGE == ?',
        whereArgs: [display],
        orderBy: ITEM_COLUMN_ORDERING,
      );

      if (maps.length <= 0) return null;

      return maps
          .map((Map<String, dynamic> map) =>
              Item.fromJson(jsonDecode(map[ITEM_COLUMN_JSON])))
          .toList();
    } catch (e) {
      print(e);
    }

    return null;
  }

  static Future<bool> updateById(int id, Item item) async {
    try {
      String json = jsonEncode(item.toJson());

      await database.update(
        TABLE_ITEM,
        {ITEM_COLUMN_JSON: json},
        where: '$ITEM_COLUMN_ITEM_ID == ?',
        whereArgs: [id],
      );

      return true;
    } catch (e) {
      print(e);
    }

    return false;
  }

  static Future<int> deleteById(int id) async {
    return await database
        .delete(TABLE_ITEM, where: '$ITEM_COLUMN_ITEM_ID = ?', whereArgs: [id]);
  }

  static Future<int> deleteByDisplay(int display) async {
    return await database.delete(TABLE_ITEM,
        where: '$ITEM_COLUMN_DISPLAY_PAGE = ?', whereArgs: [display]);
  }
}
