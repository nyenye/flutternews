import 'package:json_annotation/json_annotation.dart';
import 'package:flutternews/models/item/item.db.dart';

/// This allows the `Item` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'item.g.dart';

class ItemTypes {
  static const String Job = 'job';
  static const String Story = 'story';
  static const String Comment = 'comment';
  static const String Poll = 'poll';
  static const String PollOpt = 'pollopt';
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Item {
  Item(
    this.id,
    this.type, {
    this.title,
    this.deleted,
    this.dead,
    this.time,
    this.by,
    this.descendants,
    this.children,
    this.parent,
    this.options,
    this.poll,
    this.score,
    this.text,
    this.url,
  });

  /// The `Item`'s unique id.
  @JsonKey(required: true)
  int id;

  /// Must be one of `"job"`, `"story"`, `"comment"`, `"poll"`, `"pollopt"`.
  @JsonKey(required: true)
  String type;

  @JsonKey(required: false)
  String title;

  /// `Unix Time` of creation.
  @JsonKey(required: false)
  int time;

  /// `true` if the item is deleted.
  @JsonKey(required: false, defaultValue: false)
  bool deleted;

  /// `true` if the item is dead.
  @JsonKey(required: false, defaultValue: false)
  bool dead;

  /// The username of the `Item`'s author.
  @JsonKey(required: false, defaultValue: "Unknown")
  String by;

  @JsonKey(required: false)
  String text;

  /// The comment's parent: either another comment or the relevant story.
  @JsonKey(required: false, nullable: true)
  int parent;

  /// The pollopt's associated poll.
  @JsonKey(required: false, nullable: true)
  int poll;

  /// The pollopt's associated poll.
  // @JsonKey(required: false, defaultValue: [])
  // List<int> kids;
  @JsonKey(required: false, defaultValue: null)
  List<Item> children;

  @JsonKey(required: false, defaultValue: null)
  List<Item> options;

  @JsonKey(required: false)
  String url;

  @JsonKey(required: false)
  int score;

  // @JsonKey(required: false, defaultValue: [])
  // List<int> parts;

  @JsonKey(required: false)
  int descendants;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  factory Item.fromAlgoliaJson(Map<String, dynamic> json) =>
      _itemFromAlgoliaJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ItemToJson(this);
}

Item _itemFromAlgoliaJson(Map<String, dynamic> json) {
  return Item(
    json['id'] as int ?? int.parse(json['objectID'] as String),
    json['type'] as String ?? json['_tags'][0] as String,
    title: json['title'] as String,
    deleted: json['deleted'] as bool ?? false,
    dead: json['dead'] as bool ?? false,
    time: json['created_at_i'] as int,
    by: json['author'] as String ?? 'Unknown',
    descendants: json['children'] == null
        ? json['num_comments'] as int
        : json['children'].length,
    children: (json['children'] as List)
            ?.map((e) => e == null
                ? null
                : Item.fromAlgoliaJson(e as Map<String, dynamic>))
            ?.toList() ??
        null,
    parent: json['parent'] as int,
    options: (json['options'] as List)
            ?.map((e) => e == null
                ? null
                : Item.fromAlgoliaJson(e as Map<String, dynamic>))
            ?.toList() ??
        null,
    poll: json['poll'] as int,
    score: json['points'] as int,
    text: json['text'] as String,
    url: json['url'] as String,
  );
}
