// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Item _$ItemFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['id', 'type']);
  return Item(
    json['id'] as int,
    json['type'] as String,
    title: json['title'] as String,
    deleted: json['deleted'] as bool ?? false,
    dead: json['dead'] as bool ?? false,
    time: json['time'] as int,
    by: json['by'] as String ?? 'Unknown',
    descendants: json['descendants'] as int,
    children: (json['children'] as List)
            ?.map((e) =>
                e == null ? null : Item.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    parent: json['parent'] as int,
    options: (json['options'] as List)
            ?.map((e) =>
                e == null ? null : Item.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    poll: json['poll'] as int,
    score: json['score'] as int,
    text: json['text'] as String,
    url: json['url'] as String,
  );
}

Map<String, dynamic> _$ItemToJson(Item instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'title': instance.title,
      'time': instance.time,
      'deleted': instance.deleted,
      'dead': instance.dead,
      'by': instance.by,
      'text': instance.text,
      'parent': instance.parent,
      'poll': instance.poll,
      'children': instance.children,
      'options': instance.options,
      'url': instance.url,
      'score': instance.score,
      'descendants': instance.descendants,
    };
