import 'dart:math';
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutternews/http/fetch.dart';
import 'package:flutternews/widgets/item_peview.dart';
import 'package:flutternews/models/item/item.dart';

// https://flutter-academy.com/flutter-listview-infinite-scrolling/

class NewestItemsPage extends StatefulWidget {
  final List<int> itemIds;

  final Function onRefresh;

  NewestItemsPage(this.itemIds, this.onRefresh);

  @override
  State<StatefulWidget> createState() {
    return _NewestItemsPageState();
  }
}

const _itemsPerPage = 15;

class _NewestItemsPageState extends State<NewestItemsPage> {
  List<Item> _items = <Item>[];

  ScrollController _scrollController = ScrollController();

  int _currentPage = -1;

  bool _endReached = false;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    fetchNextPage();

    _scrollController.addListener(() {
      if (_endReached || _isLoading) return;

      if (_scrollController.position.pixels >=
          _scrollController.position.maxScrollExtent - 500.0) {
        fetchNextPage();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int listItemsCount = _isLoading ? _items.length + 1 : _items.length;

    return RefreshIndicator(
      onRefresh: _handleRefresh,
      child: ListView.builder(
        controller: _scrollController,
        itemBuilder: (BuildContext context, int index) => _buildListItem(index),
        itemCount: listItemsCount,
      ),
    );
  }

  Widget _buildListItem(int index) {
    if (index == _items.length) {
      // Render Loader:
      return Container(
        alignment: Alignment.topCenter,
        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 25.0),
        child: CircularProgressIndicator(),
      );
    }

    return ItemPreview(
      _items[index],
      index,
      showPosition: true,
    );
  }

  void fetchNextPage() async {
    setState(() {
      _isLoading = true;
    });

    _currentPage += 1;

    final int startIndex = _currentPage * _itemsPerPage;
    final int endIndex = startIndex +
        min(
          widget.itemIds.length - startIndex,
          _itemsPerPage,
        );

    if (endIndex - startIndex <= 0) {
      setState(() {
        _endReached = true;
        _isLoading = false;
      });

      return;
    }

    List<int> idsToFetch =
        widget.itemIds.getRange(startIndex, endIndex).toList();

    try {
      List<Item> itemsToAdd =
          await fetchItemsByIds(idsToFetch, endIndex - startIndex);
      setState(() {
        _items.addAll(itemsToAdd);
        _isLoading = false;
      });
    } catch (err) {
      try {
        setState(() {
          _isLoading = false;
        });
      } catch (e) {}
    }
  }

  Future<void> _handleRefresh() async {
    setState(() {
      _items = [];
      _currentPage = -1;
    });

    await widget.onRefresh();

    fetchNextPage();
  }
}
