import 'package:flutter/material.dart';
import 'package:flutternews/http/fetch.dart';

import 'package:flutternews/navigation/home_bottom_tabs.dart';
import 'package:flutternews/providers/theme.dart';

import 'package:flutternews/screens/home/page_top_items.dart';
import 'package:flutternews/screens/home/page_newest_items.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedNavIndex = 0;

  List<int> _topItemIds = [];
  List<int> _newestItemIds = [];

  @override
  void initState() {
    super.initState();

    _onRefreshTop();

    _onRefreshNewest();
  }

  @override
  Widget build(BuildContext context) {
    ThemeProvider theme = Provider.of<ThemeProvider>(context);
    Map<String, TextStyle> textStyles = theme.getThemedTextStyles;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Flutter News",
          style: textStyles['appBarTitle'],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.art_track),
            onPressed: () {
              theme.isLightTheme = !theme.isLightTheme;
            },
          ),
        ],
        // elevation: 0.0,
      ),
      body: _buildBody(theme),
      bottomNavigationBar: buildNavigationBar(
        context,
        _selectedNavIndex,
        _onNavigationTabTapped,
      ),
    );
  }

  Widget _buildBody(ThemeProvider theme) {
    Map<String, TextStyle> textStyles = theme.getThemedTextStyles;

    // Top
    if (_selectedNavIndex == 0) {
      if (_topItemIds.isNotEmpty) {
        return TopItemsPage(
          _topItemIds,
          () async => await _onRefreshTop(forceFetch: true),
        );
      }

      if (_topItemIds.isEmpty) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 25.0),
          alignment: Alignment.topCenter,
          child: CircularProgressIndicator(),
        );
      }

      return Text("Error loading Top stories");
    }

    // Newest
    if (_selectedNavIndex == 1) {
      if (_topItemIds.isNotEmpty) {
        return NewestItemsPage(
          _newestItemIds,
          () async => await _onRefreshNewest(forceFetch: true),
        );
      }

      if (_topItemIds.isEmpty) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 25.0),
          alignment: Alignment.topCenter,
          child: CircularProgressIndicator(),
        );
      }

      return Text("Error loading Newest stories");
    }

    Size size = MediaQuery.of(context).size;

    // By Topic
    return Container(
      child: Center(
        child: new Image(
          image: AssetImage('assets/images/under_construction.png'),
          width: size.width / 2,
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  void _onNavigationTabTapped(int currentIndex) {
    setState(() {
      _selectedNavIndex = currentIndex;
    });
  }

  Future<void> _onRefreshTop({bool forceFetch = false}) async {
    try {
      List<int> ids = await fetchTopItemIds(forceFetch);

      setState(() {
        _topItemIds = ids;
      });
    } catch (e) {
      _topItemIds = null;
    }
  }

  Future<void> _onRefreshNewest({bool forceFetch = false}) async {
    try {
      List<int> ids = await fetchNewestItemIds(forceFetch);

      setState(() {
        _newestItemIds = ids;
      });
    } catch (e) {
      _newestItemIds = null;
    }
  }
}
