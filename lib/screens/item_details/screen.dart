import 'package:awesome_loader/awesome_loader.dart' show AwesomeLoader;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';

import 'package:flutternews/http/fetch.dart';
import 'package:flutternews/models/item/item.dart';
import 'package:flutternews/providers/theme.dart';
import 'package:flutternews/widgets/comment.dart';
import 'package:flutternews/utils.dart';

class ItemDetailsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ItemDetailsScreenState();
  }
}

class _ItemDetailsScreenState extends State<ItemDetailsScreen> {
  Item _item;

  bool _isFetchingItem = false;
  bool _errorFetchingItem = false;

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((_) {
      final Map<String, Object> args =
          ModalRoute.of(context).settings.arguments as Map<String, Object>;

      setState(() {
        _item = args['item'] as Item;
      });

      setState(() {
        _isFetchingItem = true;
      });

      fetchItemById(_item.id)
          .then((Item item) => setState(() {
                _item = item;
                _isFetchingItem = false;
              }))
          .catchError((err) => setState(() {
                _errorFetchingItem = true;
                _isFetchingItem = false;
              }));
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_item == null) {
      return Container();
    }

    ThemeProvider theme = Provider.of<ThemeProvider>(context);
    Map<String, TextStyle> textStyles = theme.getThemedTextStyles;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Flutter News",
          style: textStyles['appBarTitle'],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.art_track),
            onPressed: () {
              theme.isLightTheme = !theme.isLightTheme;
            },
          ),
        ],
      ),
      body: _buildBody(theme),
    );
  }

  Widget _buildBody(ThemeProvider theme) {
    Map<String, Color> themeColors = theme.getThemedColors;

    List<Widget> slivers = [
      _buildHeader(theme),
      Divider(),
      _buildCommentSection(theme)
    ];

    return RefreshIndicator(
      onRefresh: _handleRefresh,
      child: CustomScrollView(
        slivers: <Widget>[
          // SliverPersistentHeader(),
          SliverList(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              return slivers[index];
            }, childCount: 3),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader(ThemeProvider theme) {
    Map<String, TextStyle> textStyles = theme.getThemedTextStyles;
    Map<String, Color> themeColors = theme.getThemedColors;

    List<Widget> stack = [];

    // Title
    if (_item.title != null && _item.title.isNotEmpty) {
      stack.add(
        Text(
          _item.title,
          style: textStyles['textL'],
        ),
      );
    }

    // URL + Browser link
    if (_item.url != null && _item.url.isNotEmpty) {
      Uri website = _item.url != null ? Uri.parse(_item.url) : null;

      stack.addAll(
        [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 6),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                website != null ? website.host : "-",
                style: textStyles['textSFaded'],
              ),
              Row(
                children: <Widget>[
                  Text(
                    FlutterI18n.translate(context, 'open_in_browser')
                        .toLowerCase(),
                    style: textStyles['textS'],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 1.5),
                  ),
                  Icon(
                    Icons.open_in_browser,
                    color: themeColors['primaryAccent'],
                    size: 16.0,
                  ),
                ],
              ),
            ],
          )
        ],
      );
    }

    // Time and User
    if (_item.time != null && _item.by != null) {
      String timeAgo = Utils.getTimeAgo(
        context,
        _item.time,
      );

      stack.addAll(
        [
          Padding(padding: EdgeInsets.symmetric(vertical: 6.0)),
          Text(
            '$timeAgo - ${_item.by}',
            style: textStyles['textSFaded'],
          )
        ],
      );
    }

    // Text content
    if (_item.text != null && _item.text.isNotEmpty) {
      stack.addAll(
        [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 6.0),
          ),
          Text(
            Utils.cleanHTMLInput(_item.text),
            style: textStyles['text'],
          )
        ],
      );
    }

    return Container(
      padding: EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: stack,
      ),
    );
  }

  Widget _buildCommentSection(ThemeProvider theme) {
    Map<String, Color> themeColors = theme.getThemedColors;

    if (_isFetchingItem) {
      return Container(
        alignment: Alignment.center,
        child: AwesomeLoader(
          loaderType: AwesomeLoader.AwesomeLoader4,
          color: themeColors['primaryAccent'],
        ),
      );
    }

    if (_errorFetchingItem) {
      return Container(
        alignment: Alignment.center,
        child: Text("Error loading item"),
      );
    }

    if (_item.children.isEmpty) {
      return Container(
        alignment: Alignment.center,
        child: Text("There are no comment."),
      );
    }

    return CommentThread(_item, 0);
  }

  Future<void> _fetchItem() async {
    setState(() {
      _isFetchingItem = true;
    });

    fetchItemById(_item.id)
        .then((Item item) => setState(() {
              _item = item;
              _isFetchingItem = false;
            }))
        .catchError((err) => setState(() {
              _errorFetchingItem = true;
              _isFetchingItem = false;
            }));
  }

  Future<void> _handleRefresh() async {
    await _fetchItem();
  }
}
