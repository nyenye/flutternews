import 'package:day/day.dart';

import 'package:flutternews/models/item/item.dart';
import 'package:flutternews/models/item/item.db.dart';
import 'package:flutternews/models/cache_refresh_times/cache_refresh_times.db.dart';

class Cache {
  static Future<List<int>> fetchItemIdsByDisplay(
    int display,
    bool forceFetch,
  ) async {
    try {
      int lastUpdate = forceFetch
          ? 0 // 0 makes date older than 1 hour ago, forcing a refresh
          : await CacheRefreshTimesDatabase.queryColumn(
              CacheRefreshTimesDatabase.getColumnByDisplay(display),
            );

      if (lastUpdate == null) return null;

      final Day cacheDate = Day.fromUnix(lastUpdate);

      if (cacheDate
          .isBefore(Day.fromDateTime(DateTime.now()).subtract(1, 'h'))) {
        CacheRefreshTimesDatabase.updateColumn(
          CacheRefreshTimesDatabase.getColumnByDisplay(display),
          value: 0,
        );
        ItemDatabase.deleteByDisplay(display);
        return null;
      }

      List<int> ids = await ItemDatabase.queryIdsByDisplay(display);

      if (ids == null || ids.isEmpty) return null;

      return ids;
    } catch (err) {
      print(err);
    }

    return null;
  }

  static Future<Item> fetchItemById(int id) async {
    try {
      Item item = await ItemDatabase.queryById(id);

      if (item == null) return null;

      return item;
    } catch (err) {
      print(err);
    }

    return null;
  }

  static Future<List<Item>> fetchItemsByIds(List<int> ids) async {
    Map<int, bool> found = {};

    try {
      List<Item> items = await ItemDatabase.queryByIds(ids);

      if (items == null) return null;

      items.removeWhere((Item item) {
        if (found[item.id] == null) {
          found[item.id] = true;
          return false;
        }
        return true;
      });

      if (items.length != ids.length) return null;

      return items;
    } catch (err) {
      print(err);
    }

    return null;
  }

  static Future<bool> cacheItemIds(
    List<int> ids,
    int display,
  ) async {
    try {
      bool insertOk = await ItemDatabase.insertBulk(ids, display);

      bool updateOk = false;
      if (insertOk) {
        updateOk = await CacheRefreshTimesDatabase.updateColumn(
          CacheRefreshTimesDatabase.getColumnByDisplay(display),
        );
      }

      return insertOk && updateOk;
    } catch (err) {
      print(err);
    }

    return false;
  }

  static Future<bool> updateCachedItemWithId(int id, Item item) async {
    try {
      bool ok = await ItemDatabase.updateById(id, item);

      return ok;
    } catch (err) {
      print(err);
    }

    return false;
  }
}
