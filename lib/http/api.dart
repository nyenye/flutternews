const hackernewsUrl = 'https://hacker-news.firebaseio.com/v0';
const algoliaUrl = 'http://hn.algolia.com/api/v1/';

final itemEndpoint = (int itemId) => 'item/$itemId.json';

final algoliaItemEndpoint = (int itemId) => 'items/$itemId';

final algoliaSearchItemsEndpoint =
    (String queryString) => 'search?$queryString';

const topStoriesEndpoint = 'topstories.json';

const newestStoriesEndpoint = 'newstories.json';

const askStoriesEndpoint = 'askstories.json';

const jobStoriesEndpoint = 'jobstories.json';

const showStoriesEndpoint = 'showstories.json';

String getAlgoliaItemsByIdQueryString(List<int> ids, int hitsPerPage) {
  String tags = _algoliaTagsIdIn(ids);

  String pagination = _algoliaPagination(hitsPerPage);

  String attributes = _algoliaAttributesFilter;

  return '$tags&$pagination&$attributes';
}

String _algoliaTagsIdIn(List<int> ids) {
  String tags = 'tags=(story,poll,show_hn,ask_hn),(';
  {
    for (int itemId in ids) tags = '${tags}story_$itemId,';
  }
  tags = '$tags)';

  return tags;
}

final _algoliaPagination =
    (int hitsPerPage) => 'page=0&hitsPerPage=$hitsPerPage';

const _algoliaAttributesFilter =
    'attributesToRetrieve=title,url,author,points,num_comments,objectID,_tags,created_at_i&attributesToHighlight';
