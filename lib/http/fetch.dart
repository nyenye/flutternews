import 'dart:convert';

import 'package:flutternews/models/item/item.db.dart';
import 'package:http/http.dart' as http;

import 'package:flutternews/http/api.dart' as api;
import 'package:flutternews/http/cache.dart';
import 'package:flutternews/models/item/item.dart';
import 'package:flutternews/models/item/item.db.dart' show ItemDisplayPage;

Future<List<int>> fetchTopItemIds(bool forceFetch) async {
  final int display = ItemDisplayPage.Top;
  List<int> cacheIds = await Cache.fetchItemIdsByDisplay(display, forceFetch);

  if (cacheIds != null) return cacheIds;

  final response =
      await http.get('${api.hackernewsUrl}/${api.topStoriesEndpoint}');

  if (response.statusCode == 200) {
    List<dynamic> json = jsonDecode(response.body);

    List<int> ids = json.map((dynamic id) => id as int).toList();
    await Cache.cacheItemIds(ids, display);

    return ids;
  } else
    throw Exception('Failed to load top items');
}

Future<List<int>> fetchNewestItemIds(bool forceFetch) async {
  final int display = ItemDisplayPage.Newest;
  List<int> cacheIds = await Cache.fetchItemIdsByDisplay(display, forceFetch);

  if (cacheIds != null) return cacheIds;

  final response =
      await http.get('${api.hackernewsUrl}/${api.newestStoriesEndpoint}');

  if (response.statusCode == 200) {
    List<dynamic> json = jsonDecode(response.body);

    List<int> ids = json.map((dynamic id) => id as int).toList();
    await Cache.cacheItemIds(ids, display);

    return ids;
  } else
    throw Exception('Failed to load newest items');
}

Future<Item> fetchItemById(int id, {forceFetch = false}) async {
  if (!forceFetch) {
    Item cacheItem = await Cache.fetchItemById(id);

    print(cacheItem.children.length);

    if (cacheItem != null &&
        cacheItem.children != null &&
        cacheItem.children.length > 0) {
      return cacheItem;
    }
  }

  final response =
      await http.get('${api.algoliaUrl}/${api.algoliaItemEndpoint(id)}');

  if (response.statusCode == 200) {
    Map<String, dynamic> json = jsonDecode(response.body);

    Item item = Item.fromAlgoliaJson(json);

    await Cache.updateCachedItemWithId(id, item);

    return item;
  } else
    throw Exception('Failed to load item by ID');
}

Future<List<Item>> fetchItemsByIds(List<int> ids, int itemsPerPage) async {
  List<Item> cacheItems = await Cache.fetchItemsByIds(ids);

  if (cacheItems != null) return cacheItems;

  String queryString = api.getAlgoliaItemsByIdQueryString(ids, itemsPerPage);

  final response = await http
      .get('${api.algoliaUrl}/${api.algoliaSearchItemsEndpoint(queryString)}');

  Map<String, dynamic> json = jsonDecode(response.body);

  List<Item> items = [];

  for (Map<String, dynamic> hit in json['hits']) {
    Item item = Item.fromAlgoliaJson(hit);
    items.add(item);
    await Cache.updateCachedItemWithId(item.id, item);
  }

  return items;
}
