import 'package:flutter/material.dart';

import 'package:flutter_i18n/flutter_i18n_delegate.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'package:flutternews/sqlite/index.dart' show initSQLite;
import 'package:flutternews/navigation/routes.dart' as Routes;
import "package:flutternews/providers/theme.dart";

void main() async {
  final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
      useCountryCode: false,
      fallbackFile: 'en',
      path: 'assets/i18n',
      forcedLocale: new Locale('en'));
  WidgetsFlutterBinding.ensureInitialized();
  await flutterI18nDelegate.load(null);

  await initSQLite();

  ThemeProvider themeProvide = ThemeProvider();

  runApp(
    ChangeNotifierProvider(
      create: (_) => themeProvide,
      builder: (_) => themeProvide,
      child: MyApp(flutterI18nDelegate),
    ),
  );
}

class MyApp extends StatelessWidget {
  final FlutterI18nDelegate flutterI18nDelegate;

  MyApp(this.flutterI18nDelegate);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.initialRoute,
      routes: Routes.map,
      title: 'FlutterNews',
      theme: themeProvider.getThemeData,
      localizationsDelegates: [
        flutterI18nDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
    );
  }
}
